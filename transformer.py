import pandas as pd
from rdflib import Graph, URIRef, Literal, Namespace
from rdflib.namespace import RDF, XSD
import os
import sys

# Check if a file path is provided
if len(sys.argv) != 2:
    print("Could not run the script.\nUsage: python transformer.py path/to/dataset.csv")
    sys.exit(1)

# Input file path
file_path = sys.argv[1]
print(f"Reading dataset from: {file_path}")

# Read the dataset
data = pd.read_csv(file_path)
data = data.head(100)
print(f"Dataset loaded successfully. Number of records: {len(data)}\nBeginning RDF conversion...")

# Define namespace
SAREF = Namespace('https://saref.etsi.org/')

# Create an RDF graph and load the SAREF ontology
graph = Graph()
graph.bind('saref', SAREF)

# Define base URI for our dataset
link = 'http://example.org/household_data/'

# Transform the dataset into an RDF graph using properties and classes defined in the SAREF ontology
for index, row in data.iterrows():
    # Add measurement resource
    measurement_uri = URIRef(f'{link}_Measurement/{index + 1}')
    
    # Add type for measurement
    graph.add((measurement_uri, RDF.type, SAREF.Measurement))
    
    # Add timestamp of when measurement was taken
    graph.add((measurement_uri, SAREF.hasTimestamp, Literal(row['cet_cest_timestamp'], datatype=XSD.dateTime)))

    # Iterate over each column to handle different device readings, excluding timestamps and interpolated
    for column in data.columns[2:-1]:
        if data.loc[index].notna()[column]: 
            graph.add((measurement_uri, SAREF[column], Literal(row[column], datatype=XSD.float)))

# Serialize the resulting RDF graph in Turtle format


output = os.path.join(os.path.dirname(__file__), 'graph.ttl')
graph.serialize(destination=output, format='turtle')
print(f"RDF graph has been serialized to {output}")
